public class Student {
    double[] Marks = new double[5];
    double MiddleMark;
    int GroupNumber;
    String Name;



    public double getMiddleMark() {
        return MiddleMark;
    }

    public Student(String Name, int GroupNumber, double Mark1, double Mark2, double Mark3, double Mark4, double Mark5){
        this.Name = Name;
        this.GroupNumber = GroupNumber;
        this.Marks[0] = Mark1;
        this.Marks[1] = Mark2;
        this.Marks[2] = Mark3;
        this.Marks[3] = Mark4;
        this.Marks[4] = Mark5;
        MiddleMark =  (Marks[0] + Marks[1] + Marks[2] + Marks[3] + Marks[4])/5;
    }
}
