import java.util.*;

public class Main {
    public static void swap(List<Student> list,int i)
    {
        Student tmp = list.get(i);
        list.set(i,list.get(i+1));
        list.set(i+1,tmp);
    }
    public static void main(String[] args) {
        List<Student> students = new ArrayList<>();
        students.add(new Student("Андрей Витальевич ",1,5,5,4,3,4));
        students.add(new Student("Василий Еремеевич ",1,2,3,4,3,3));
        students.add(new Student("Олег Иванович ",1,5,5,4,2,2));
        students.add(new Student("Виктор Александрович ",2,5,5,5,5,5));
        students.add(new Student("Александр Сергеевич ",1,2,3,3,3,4));
        students.add(new Student("Павел Константинович ",2,3,5,3,4,4));
        students.add(new Student("Иван Иванович ",3,4,5,4,4,4));
        students.add(new Student("Григорий Андреевич ",3,3,2,4,3,3));
        students.add(new Student("Денис Багратович ",3,4,4,3,2,2));
        students.add(new Student("Баграт Игоревич ",3,4,3,2,3,5));


        while(true) {
            System.out.println("\n1-Просмотр списка номеров групп студента \n2 - Посмотреть средний балл \n3 - Просмотреть студентов - отличников и студентов - хорошистов");
            Scanner s = new Scanner(System.in);
            int p = s.nextInt();
            switch (p) {
                case 1:
                    for (int i = 0; i < 10; i++)
                        System.out.println(students.get(i).Name + "Группа студента:" + students.get(i).GroupNumber);
                    break;
                case 2:
                    List<Student> LS = new ArrayList<>();
                    int i = 0;
                    for (Student item : students) {
                        LS.add(item);
                        i++;
                    }
                    for (int start = 0; start < LS.size() - 1; start++) {
                        for (int index = 0; index < LS.size() - 1 - start; index++) {
                            if (LS.get(index).MiddleMark > LS.get(index + 1).MiddleMark) {
                                swap(LS, index);
                            }
                        }
                    }
                    students = LS;

                    for (int u = 0; u < 10; u++)
                        System.out.println(students.get(u).Name + "Средний балл студента:" + students.get(u).MiddleMark);
                    break;
                case 3:

                    for (int q = 0; q < 10; q++) {
                        int qwe = 1;
                        for (int g = 0; g < students.get(q).Marks.length - 1; g++) {
                            if (students.get(q).Marks[g] >= 4) {
                                qwe++;
                                if (qwe == 5) {
                                    System.out.println(students.get(q).Name + "Группа студента: " + students.get(q).GroupNumber);
                                }
                            }


                        }


                    }
                    break;
                }
            }




        }
    }

