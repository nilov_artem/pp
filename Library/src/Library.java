import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Library {

    public static void main(String[] args) {
        List<Books> list = new ArrayList<>();
        int id = 0;
        while (true){
            Scanner scan = new Scanner(System.in);
            System.out.println("1-Добавить книгу\n2-Найти книгу\n3-Удалить книгу");
            int j = scan.nextInt();
            if (j > 3 || j < 0)
                break;
            switch (j){
                case 1:
                    list.add(new Books(id, Books.AddToLibrary("Название:"), Books.AddToLibrary("Автор:"), Integer.valueOf(Books.AddToLibrary("Год издания:"))));
                    id++;
                    break;
                case 2:
                    if (list.size() == 0) {
                        System.out.println(list.size());
                        System.out.println("Книг нет");
                        continue;
                    }
                    else {
                        System.out.println("\n1-Найти книгу по названию\n2 - Найти книгу по автору\n3-Найти книгу по году издания\n");
                        int v = scan.nextInt();
                        if (v > 3 || v < 0)
                            break;
                        switch (v){
                            case 1:
                                System.out.print("Введите название книги: ");
                                String name = scan.next();
                                for (int i = 0; i < list.size(); i++) {
                                    if (list.get(i).Name == name)
                                        System.out.println("Номер книги: " + (list.get(i).ID) + " Автор: " + list.get(i).Author + " Название: " + list.get(i).Name + " Год издания " +list.get(i).Year);
                                }
                                System.out.println();
                                break;
                            case 2:
                                System.out.print("Введите автора книги: ");
                                String author = scan.next();
                                for (int i = 0; i < list.size(); i++) {
                                    if (list.get(i).Author == author)//проверка на автора
                                        System.out.println("Номер книги: " + (list.get(i).ID) + " Автор: " + list.get(i).Author + " Название: " + list.get(i).Name + " Год издания: " + list.get(i).Year);
                                }
                                System.out.println();
                                break;
                            case 3:
                                System.out.print("Введите год издания книги: ");
                                int year = scan.nextInt();
                                for (int i = 0; i < list.size(); i++) {
                                    if (list.get(i).Year == year)
                                        System.out.println("Номер книги: " + (list.get(i).ID) + " Автор: " + list.get(i).Author +  " Название: " + list.get(i).Name + " Год издания: " + list.get(i).Year);
                                }
                                System.out.println();
                                break;
                        }
                        break;
                    }
                case 3:
                    if (list.size() == 0) {
                        System.out.println(list.size());
                        System.out.println("Книг нет");
                        continue;
                    }
                    else{
                        System.out.println("Введите номер удаляемой книги:");
                        int a = scan.nextInt();
                        if (a > list.size() || a < 0)
                            break;
                        list.remove(a);
                    }
                    break;
            }
        }
    }
}
