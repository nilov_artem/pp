import java.util.Scanner;
public class Books {

    int ID;
    String Name;
    String Author;
    int Year;

    public Books(int ID, String Name, String Author, int Year){
        this.ID = ID;
        this.Name = Name;
        this.Author = Author;
        this.Year = Year;
    }

    public static String AddToLibrary(String message) {
        Scanner sc = new Scanner(System.in);
        System.out.println(message);
        return sc.next();
    }

}
