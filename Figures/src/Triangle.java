public class Triangle extends  Figure {
    private double a;
    private double b;
    private double c;

    public Triangle(double a, double b, double c){
        this.a = a;
        this.b = b;
        this.c = c;

    }


    public double getA() {
        return a;
    }

    public double getB() {
        return b;
    }

    public double getC() {
        return c;
    }

    @Override
    public double Perimeter() {
        return a+b+c;
    }

    @Override
    public double Area() {
        double p = (a+b+c)/2;//полупериметр
        double s = Math.sqrt(p*(p-a)*(p-b)*(p-c));//формула Герона
        return s;
    }
}
