public class Rectangle extends Figure{
    private double a;
    private double b;

    public Rectangle(double a, double b){
        this.a = a;
        this.b = b;
    }

    public double getA() {
        return a;
    }

    public double getB() {
        return b;
    }

    @Override
    public double Perimeter() {
        return 2*(a+b);
    }

    @Override
    public double Area() {
        return a*b;
    }
}
