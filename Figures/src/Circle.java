public class Circle extends Figure {
    private double radius;

    public Circle(double radius){
        this.radius = radius;
    }

    public double getR() {
        return radius;
    }

    @Override
    public double Perimeter() {
        return radius *(2*Math.PI);
    }

    @Override
    public double Area() {
        return (radius*Math.PI)*(radius*Math.PI);
    }
}
