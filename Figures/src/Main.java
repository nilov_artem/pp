import java.util.*;
public class Main {
    public static void main(String[] args) {
        List<Figure> Figures = new ArrayList<>();
        while(true){
            Scanner scan = new Scanner(System.in);
            System.out.println("\n1-Добавить фигуру\n2-Просмотреть фигуры\n");
            int j = scan.nextInt();
            switch (j){
                case 1:
                    System.out.println("\n1-Треугольник \n2-Круг \n3-Прямоугольник\n");
                    Scanner s = new Scanner(System.in);
                    int k = s.nextInt();
                    switch (k){
                        case 1:
                            Figures.add(new Triangle(Double.valueOf(Figure.Reader("Первая сторона")),Double.valueOf(Figure.Reader("Вторая сторона")),Double.valueOf(Figure.Reader("Третья сторона"))));
                            break;
                        case 2:
                            Figures.add(new Circle(Double.valueOf(Figure.Reader("Введите радиус"))));
                            break;
                        case 3:
                            Figures.add(new Rectangle(Double.valueOf(Figure.Reader("Первая сторона")),Double.valueOf(Figure.Reader("Вторая сторона"))));
                            break;

                    }
                    break;
                case 2:
                    if (Figures.size() == 0) {
                       System.out.println("Список пуст");

                    }
                    else {
                        Figure.Parametrs(Figures);
                    }
                    break;
            }
        }
    }
}
