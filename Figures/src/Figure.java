import java.util.*;

abstract public class Figure {
    abstract public double Perimeter();
    abstract  public double Area();

    public static String Reader(String message) {
        Scanner sc = new Scanner(System.in);
        System.out.println(message);
        return sc.next();
    }

    public static void Parametrs(List<Figure> a){
        int i = 0;
        for (Figure pr : a) {
            if (pr instanceof Triangle) {
                System.out.println("Номер фигуры:" + (i) + "Тип фигуры: Треугольник " + " Первая сторона: " + ((Triangle) pr).getA() + " Вторая сторона: " + ((Triangle) pr).getB() +  " Третья сторона:" + ((Triangle) pr).getC() + " Периметр: "+pr.Perimeter()+" Площадь: "+pr.Area()+"\n");
            }
            if (pr instanceof Rectangle) {
                System.out.println("Номер фигуры:" + (i) + "Тип фигуры: Прямоугольник " + " Первая сторона: " + ((Rectangle) pr).getA() + "Вторая сторона: " + ((Rectangle) pr).getB() +" Периметр: "+pr.Perimeter()+" Площадь: "+pr.Area()+"\n");
            }
            if (pr instanceof Circle) {
                System.out.println("Номер фигуры:" + (i) + "Тип фигуры: Круг " + " Радиус: " + ((Circle) pr).getR() +" Периметр: "+pr.Perimeter()+" Площадь: "+pr.Area()+"\n");
            }
            i++;
        }
    }
}
